/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tp2.POJO;

/**
 *
 * @author bdejoie
 */
/**********************************************
* Exercice 3
* Commentaire : Exercice fini 
***********************************************/
public class Adress {
    int id;
    int num;
    String street1;
    String street2;
    String country;
    String city;
    int postal_code;
    double latitude;
    double longitude;

    public Adress(int num, String street1,String street2,String country,String city,int postal_code,double latitude,double longitude) { //constructeur avec le tout
        this.num = num;
        this.street1 = street1;
        this.street2 = street2;
        this.country = country;
        this.city = city;
        this.postal_code = postal_code;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    public Adress(int num, String street1,String street2,String country,String city,int postal_code) { // constructeur sans latitude et longitude
        this.num = num;
        this.street1 = street1;
        this.street2 = street2;
        this.country = country;
        this.city = city;
        this.postal_code = postal_code;
    }
    
    public String toString(){ //override de la méthode toString pour afficher de cette facon
        return "L'adresse est " + num + " " + street1 + " "+ street2 + " " + city + " " + country + " qui se situe aux coordonnées" + latitude + " : " + longitude;
    }
}
