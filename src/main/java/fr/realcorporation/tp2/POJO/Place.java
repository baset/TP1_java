/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tp2.POJO;

/**
 *
 * @author bdejoie
 */
/**********************************************
* Exercice 4
* Commentaire : Exercice non fini problème avec l'énumération 
***********************************************/
public class Place {
    int num;
    String name;
    String address;
    String country;
    String note;
    Type type;

    public Place(int num, String name,String address,String country,String note,Type type) { //constructeur avec le tout
        this.num = num;
        this.name = name;
        this.address = address;
        this.country = country;
        this.note = note;
        this.type = type;
    }

    
    public String toString(){
        return "Le lieu est : " + name + " " + address + " " + country + " " + note + " et il s'agit : " + type;
    }
    
    public enum Type {
        ecole("une école"), 
        cinema("un cinéma"), 
        bibliothèque("une bibliothèque");
        private String name="";
        
        Type(String name){
            this.name=name;
        }
        public String toString(){
            return name;
        }
    }
}
