/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tp2.POJO;

/**
 *
 * @author bdejoie
 */
/**********************************************
* Exercice 6
* Commentaire : Exercice fini
***********************************************/
public class Teacher extends Person{
    String summary;
    
    public Teacher(int num, String name, String surname, Adress address, int age, String summary) {
        super(num, name, surname, address, age);
        this.summary = summary;
    }
    public String toString(){
        return "Vous êtes le professeur : " + name + " " + surname + " habitant à " + address + " et vous avez : " + age + " et vous enseignez la matière : " + summary;
    }
}
