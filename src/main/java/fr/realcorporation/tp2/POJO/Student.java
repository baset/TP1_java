/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tp2.POJO;

/**
 *
 * @author bdejoie
 */
/**********************************************
* Exercice 6
* Commentaire : Exercice fini
***********************************************/
public class Student extends Person {
    String classe;
    
    public Student(int num, String name, String surname, Adress address, int age, String classe) {
        super(num, name, surname, address, age);
        this.classe = classe;
    }
    public String toString(){
        return "Vous êtes l'élève : " + name + " " + surname + " habitant à " + address + " et vous avez : " + age + " et vous êtes en : " + classe;
    }
}
