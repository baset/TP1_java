/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tp2.POJO;

/**
 *
 * @author bdejoie
 */
/**********************************************
* Exercice 5
* Commentaire : Exercice fini
***********************************************/
public class Person {
    int num;
    String name;
    String surname;
    Adress address;
    int age;

    public Person(int num, String name,String surname,Adress address, int age) { //constructeur avec le tout
        this.num = num;
        this.name = name;
        this.address = address;
        this.surname = surname;
        this.age = age;
    }
    
    public String toString(){
        return "Vous êtes : " + name + " " + surname + " habitant à " + address + " et vous avez : " + age ;
    }
}
