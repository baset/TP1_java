/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tp2;

import fr.realcorporation.tp2.POJO.Adress;
import fr.realcorporation.tp2.POJO.Person;
import fr.realcorporation.tp2.POJO.Place;
import fr.realcorporation.tp2.POJO.Place.Type;
import fr.realcorporation.tp2.POJO.Student;
import fr.realcorporation.tp2.POJO.Teacher;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author bdejoie
 */
/**********************************************
* Exercice 0 et 2
* Commentaire : Exercice fini 
***********************************************/

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());
    public static void main(final String[] testArg){ 
        Scanner sc = new Scanner(System.in); // on déclare un scanner pour la saisie
        System.out.println("Veuillez saisir un nombre (saisir 0 pour sortir) :"); // saisir un nombre
        int str = sc.nextInt(); 
        while(str != 0){ // boucle de saisie tant que le nombre saisie n'est pas 0
            testArg[0] = "Le résultat de la factorielle est : "+ factorielle(str);
            LOGGER.log( Level.SEVERE, testArg[0]);
            System.out.println("Veuillez saisir un nombre (saisir 0 pour sortir) :");
            str = sc.nextInt();   
        }
        /**********************************************
        * Exercice 3
        * Commentaire : Exercice fini 
        ***********************************************/
        Adress address1; //adresse de demonstration
        address1 = new Adress(12,"cour", "du truc", "pays du fromage", "chateauroux", 36000, 5000, 5000 );
        LOGGER.log( Level.SEVERE, address1.toString()); //affichage dans la console de l'adresse
        
        /**********************************************
        * Exercice 4
        * Commentaire : Exercice non fini problème avec l'énumération
        ***********************************************/
        Place place1; //place de demonstration
        place1 = new Place(12,"Jean Valjean", address1.toString(), "pays du fromage", "c'est une note ca", Type.ecole ); //pas d'énumération sinon ca marche pas
        LOGGER.log( Level.SEVERE, place1.toString());
        
        /**********************************************
        * Exercice 5
        * Commentaire : Exercice fini
        ***********************************************/
        Person person1; //personne de demonstration
        person1 = new Person(12,"testPerson", "testPerson", address1, 15 ); 
        LOGGER.log( Level.SEVERE, person1.toString());
        
        /**********************************************
        * Exercice 6
        * Commentaire : Exercice fini
        ***********************************************/
        Teacher teacher1; //prof de demonstration
        teacher1 = new Teacher(12,"Jean", "Valjean", address1, 51, "Mathématiques" ); 
        LOGGER.log( Level.SEVERE,teacher1.toString());
        Student student1; //élève de demonstration
        student1 = new Student(12,"Robert", "Van Der Burg", address1, 15 , "5ème" );
        LOGGER.log( Level.SEVERE, student1.toString());
    }
    /**********************************************
    * Exercice 1
    * Commentaire : Exercice fini
    ***********************************************/
    public static int factorielle(int nombre){ //on calcul le factorielle d'un nombre passé en paramètre
        int i = 0;
        int resultat = 1;
        while(i<nombre){ // n! = 1*2*3* ... * n
            resultat = i * (i+1); 
            i++;
        }
        return resultat;
    }
    
    
} 
