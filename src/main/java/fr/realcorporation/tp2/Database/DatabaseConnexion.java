/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.TP2.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author rthigoulet
 */
public class DatabaseConnexion {

    public static Connection connexion() {
        try {

            String db = "127.0.0.1";
            String host = "localhost";
            String port = "3306";
            String dbname = "TP1_java";
            String user = "root";
            String pw = "";

            Connection con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + dbname + "?user=" + user + "&password=" + pw + "&serverTimezone=Europe/Paris");

            return con;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ResultSet query(String query) {
        try {
            Connection conn = DatabaseConnexion.connexion();
            Statement statement = conn.createStatement();
            ResultSet resultat = null;
            if(statement.execute(query)){
                resultat = statement.getResultSet();
            }
            
            return resultat;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
