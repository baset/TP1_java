-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 17 jan. 2018 à 12:07
-- Version du serveur :  10.1.25-MariaDB
-- Version de PHP :  7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tp1_java`
--

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

CREATE TABLE `adresse` (
  `id` int(10) NOT NULL,
  `num` int(10) NOT NULL,
  `street1` varchar(70) NOT NULL,
  `street2` varchar(70) NOT NULL,
  `city` varchar(70) NOT NULL,
  `country` varchar(70) NOT NULL,
  `postal_code` int(5) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `adresse`
--

INSERT INTO `adresse` (`id`, `num`, `street1`, `street2`, `city`, `country`, `postal_code`, `latitude`, `longitude`) VALUES
(1, 12, 'Cour du roulage', '', 'Chateauroux', 'France', 36000, 50000, 50000),
(2, 36, 'route du truc', 'au bidule', 'Chateauroux', 'France', 36000, 75000, 75000);

-- --------------------------------------------------------

--
-- Structure de la table `lieu`
--

CREATE TABLE `lieu` (
  `num` int(10) NOT NULL,
  `name` varchar(70) NOT NULL,
  `type` varchar(70) NOT NULL,
  `note` varchar(255) NOT NULL,
  `adresse` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `lieu`
--

INSERT INTO `lieu` (`num`, `name`, `type`, `note`, `adresse`) VALUES
(1, 'jean Valjean', 'une école', 'ca c\'est une note', 1),
(2, 'Robert Van Der Burg', 'une école', 'ca c\'est une note', 2);

-- --------------------------------------------------------

--
-- Structure de la table `person`
--

CREATE TABLE `person` (
  `num` int(10) NOT NULL,
  `name` varchar(70) NOT NULL,
  `surname` varchar(70) NOT NULL,
  `lieu` int(10) NOT NULL,
  `age` int(3) NOT NULL,
  `bool` int(1) NOT NULL,
  `cours` varchar(70) NOT NULL,
  `classe` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `person`
--

INSERT INTO `person` (`num`, `name`, `surname`, `lieu`, `age`, `bool`, `cours`, `classe`) VALUES
(1, 'Fontaine', 'Clément', 1, 51, 0, 'nulité', ''),
(2, 'Munoz', 'Alex', 2, 16, 1, '', '6ème'),
(3, 'Pradet', 'Quentin', 1, 14, 1, '', '2nde'),
(4, 'Thigoulet', 'Romain', 2, 40, 0, 'la noobitude', ''),
(5, 'Dejoie', 'Bastien', 1, 22, 0, 'proffesionnalisme', ''),
(6, 'Da Cruz', 'Cristophe', 2, 40, 0, 'magicarpisme', ''),
(7, 'Truc', 'Roger', 0, 13, 1, '', '1ère'),
(8, 'Bidule', 'Robert', 0, 14, 1, '', '5ème');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `lieu`
--
ALTER TABLE `lieu`
  ADD PRIMARY KEY (`num`);

--
-- Index pour la table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`num`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `adresse`
--
ALTER TABLE `adresse`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `lieu`
--
ALTER TABLE `lieu`
  MODIFY `num` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `person`
--
ALTER TABLE `person`
  MODIFY `num` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
